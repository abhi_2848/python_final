import tkinter as tk
from datetime import datetime, timedelta 
    
em_id = input("Enter your Punch in I'd: ")
name = input("Enter your name: ") 
        
def write_slogan():
    print("Hello!")
    print(name, '-', em_id)
    current_time = datetime.now()
    '{:%H:%M:%S}'.format(current_time)    
    print("Punch In time: ", current_time)
    new_time = datetime.now() + timedelta(hours=4)
    print("Your punch out time will be: ", new_time)
    
def w_slogan():
    print("Thank You!,name)
    print("Have a nice time")
    current_time = datetime.now()
    '{:%H:%M:%S}'.format(current_time)
    print("Punch out time: ", current_time)

window=tk.Tk()
window.title("Punch Clock")
frame = tk.Frame(window)
frame.pack()

window.rowconfigure(0, minsize=50, weight=1)
window.columnconfigure([0,1,2], minsize=56, weight=1)

button = tk.Button(frame,
                   text="Punch In", 
                   fg="green",
                   command=write_slogan)
button.pack(side=tk.LEFT)
slogan = tk.Button(frame,
                   text="Punch Out",
                   fg="red",
                   command=w_slogan)
slogan.pack(side=tk.LEFT)
window.mainloop()
            
                   

